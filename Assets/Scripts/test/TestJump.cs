using DG.Tweening;
using UnityEngine;

public class TestJump : MonoBehaviour
{
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Vector3 randomPosition = GetRandomPosition();
            Debug.Log(randomPosition);
            transform.DOJump(randomPosition, 1.5f, 2, 1).SetEase(Ease.Linear);
        }
    }

    private Vector3 GetRandomPosition()
    {
        Vector2 positionInCirle = Random.insideUnitCircle * 2;

        var objectPosition = transform.position;
        Vector3 randomPosition = objectPosition + new Vector3(positionInCirle.x, 0, positionInCirle.y);
        return randomPosition;
    }
}
