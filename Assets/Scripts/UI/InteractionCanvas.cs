using System;
using System.Collections.Generic;
using System.Linq;
using StaticVariables;
using StaticVariables.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class InteractionCanvas : MonoBehaviour
    {
        [SerializeField] private ScriptableObject test;
        [SerializeField] private List<HarvestableTypeSprites> _harvestableTypeSprites;
        [SerializeField] private List<FarmlandStateSprites> _farmlandStateSprites;
        [SerializeField] private Button actionButton;
        [SerializeField] private Image actionButtonImage;

        private Action _currentActionToPerform;

        private void Start()
        {
            actionButton.onClick.AddListener(InvokeActionToPerform);
        }

        public void SetActionToPerform(HarvestableType harvestableType, Action actionToPerform,
            FarmlandState farmlandState = FarmlandState.Empty)
        {
            SetSpriteForButton(harvestableType, farmlandState);

            _currentActionToPerform = actionToPerform;
            
            actionButton.gameObject.SetActive(true);
        }

        public void DeactivateAction()
        {
            _currentActionToPerform = null;
            
            actionButton.gameObject.SetActive(false);
        }
        
        private void InvokeActionToPerform() => _currentActionToPerform?.Invoke();
        private void SetSpriteForButton(HarvestableType harvestableType, FarmlandState farmlandState)
        {
            if (farmlandState is FarmlandState.Seed or FarmlandState.Empty)
            {
                actionButtonImage.sprite = _harvestableTypeSprites.First(
                    i => i.harvestableType == harvestableType).sprite;
            }
            else
            {
                actionButtonImage.sprite = _farmlandStateSprites.First(
                    i => i.farmlandState == farmlandState).sprite;
            }
        }
        
        [Serializable]
        private class HarvestableTypeSprites
        {
            public Sprite sprite;
            public HarvestableType harvestableType;
        }
        
        [Serializable]
        private class FarmlandStateSprites
        {
            public Sprite sprite;
            public FarmlandState farmlandState;
        }
    }
}
