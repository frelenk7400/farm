using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using StaticVariables;
using StaticVariables.Enums;

namespace Interactable.Crops
{
    public class Crop : IInteractable
    {
        private readonly List<GameObject> _growStages = new List<GameObject>(); 
        private readonly GameObject _cropObject;
        private readonly GameObject _cropLootObject;
        private readonly HarvestableType _harvestableType;
        private int _currentStage;
        
        public Crop(GameObject cropObject, GameObject cropLootObject, HarvestableType harvestableType)
        {
            _cropObject = cropObject;
            _cropLootObject = cropLootObject;
            _harvestableType = harvestableType;
            
            foreach (Transform child in _cropObject.transform)
                _growStages.Add(child.gameObject);
            
            Reset();
        }

        public void Interact()
        {
            if (_currentStage == _growStages.Count)
                Gather();
            else
                ChangeGrowStage();
            
            _currentStage++;
        }
        
        private void Gather()
        {
            _cropObject.SetActive(false);
        }

        private void ChangeGrowStage()
        {
            Vector3 endPosition = _cropObject.transform.position;
            Vector3 startPosition = endPosition - Vector3.up * .5f;
            
            if (_currentStage >= 1)
            {
                GameObject previousGameObject = _growStages[_currentStage - 1];
                AppearanceAnimation(_growStages[_currentStage - 1].transform, endPosition, startPosition,
                    delegate { previousGameObject.SetActive(false); });
            }

            _growStages[_currentStage].SetActive(true);
            AppearanceAnimation(_growStages[_currentStage].transform, startPosition, endPosition);
        }

        public void AppearanceAnimation(Transform animateTo, Vector3 startPosition, Vector3 endPosition,
            Action actionAfterAnimation = null)
        {
            animateTo.position = startPosition;
            animateTo.DOMove(endPosition, Variables.CropsAnimationTime).SetEase(Ease.Linear)
                .OnComplete(delegate { actionAfterAnimation?.Invoke(); });
        }

        private void Reset()
        {
            _currentStage = 0;
            
            foreach (var growStage in _growStages)
            {
                growStage.transform.position = _cropObject.transform.position;
                growStage.SetActive(false);
            }
            
            _growStages[0].SetActive(true);
        }
    }
}
