using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interactable.Crops;
using Managers;
using StaticVariables;
using StaticVariables.Enums;
using UI;
using UnityEngine;
using Zenject;

public class Field : MonoBehaviour
{
    [SerializeField] private HarvestableType harvestableType;
    [SerializeField] private List<Transform> farmlands;
    
    private Dictionary<Transform, FarmlandState> _farmlandsStates = new();
    private Dictionary<Transform, CropCreationManager.CropData> _farmlandsCrops = new();

    private CropCreationManager _creationManager;
    private InteractionCanvas _interactionCanvas;
    
    private FarmlandState currentState = FarmlandState.Seed;

    private bool interactionGranted;
    
    const int FarmDistance = 10;
    
    [Inject]
    public void Construct(CropCreationManager creationManager, InteractionCanvas interactionCanvas)
    {
        _creationManager = creationManager;
        _interactionCanvas = interactionCanvas;
    }

    private void Awake()
    {
        foreach (var field in farmlands)
            _farmlandsStates.Add(field,FarmlandState.Seed);
    }

    private void OnTriggerEnter(Collider other) => StartCoroutine(SetActionForButton());
    
    private void OnTriggerExit(Collider other) => _interactionCanvas.DeactivateAction();

    private void OnTriggerStay(Collider other)
    {
        if (!interactionGranted)
            return;
        
        Vector3 playerInteractionPos = other.gameObject.transform.TransformPoint(other.transform.forward);
        
        InteractWithCrops(playerInteractionPos);
    }

    private async void InteractWithCrops(Vector3 interactionPosition)
    {
        for (int i = 0; i < _farmlandsStates.Count; i++)
        {
            Transform farmlandKey = _farmlandsStates.ElementAt(i).Key;

            float playerToFarmlandDistance = Vector3.Distance(farmlandKey.position, interactionPosition);

            if (_farmlandsStates[farmlandKey] == currentState &&
                playerToFarmlandDistance < FarmDistance)
            {
                _farmlandsStates[farmlandKey] = currentState + 1;

                var farmlandPosition = farmlandKey.position;
                
                if (currentState == FarmlandState.Seed)
                {
                    var cropData = _creationManager.GetCrop(harvestableType);
                   // IInteractable crop = new Crop();

                   _farmlandsCrops.Add(farmlandKey, cropData);
                    
                    cropData.cropGameObject.transform.position = farmlandPosition;
                }

                _farmlandsCrops[farmlandKey].interactable.Interact();
            }
        }
        
        IsAllCropsInteracted();
    }
    
    private void IsAllCropsInteracted()
    {
        foreach (var field in _farmlandsStates)
        {
            if (field.Value == currentState)
            {
                return;
            }
        }

        interactionGranted = false;
        
        if (currentState == FarmlandState.Gather)
            ResetFarmlands();
        else
            currentState++;
        
        StartCoroutine(SetActionForButton(Variables.CropsAnimationTime));
    }

    private void ResetFarmlands()
    {
        for (int i = 0; i < _farmlandsStates.Count; i++)
        {
            var farmlandKey = _farmlandsStates.ElementAt(i).Key;
            _farmlandsStates[farmlandKey] = FarmlandState.Seed;
        }

        foreach (var cropData in _farmlandsCrops.Values)
        {
            _creationManager.ReleaseCropGameObject(harvestableType,cropData.cropGameObject);
        }
        
        _farmlandsCrops.Clear();
        
        currentState = FarmlandState.Seed;
    }

    private IEnumerator SetActionForButton(float delayForAction = 0)
    {
        yield return new WaitForSeconds(delayForAction);
        _interactionCanvas.SetActionToPerform(harvestableType,
            delegate { interactionGranted = true; }, currentState);
    }
    
  
}
