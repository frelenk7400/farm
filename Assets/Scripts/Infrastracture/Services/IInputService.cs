using System;
using UnityEngine;

namespace Infrastracture.Services
{
    public interface IInputService
    {
        public event Action<Vector2> OnAxisChange;
        public event Action OnAxisButtonUp;
        public event Action OnAttackButtonPress;
    }
}

