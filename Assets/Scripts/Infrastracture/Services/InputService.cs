using System;
using UnityEngine;
using Zenject;

namespace Infrastracture.Services
{
    public class InputService : IInputService, ITickable
    {
        private static string attackButtonName = "Attack";

        public event Action<Vector2> OnAxisChange;
        public event Action OnAxisButtonUp;
        public event Action OnAttackButtonPress;

        private void CheckAxisChange()
        {
            Vector2 axisInput = new Vector2(SimpleInput.GetAxis("Horizontal"), SimpleInput.GetAxis("Vertical"));
        
            if (axisInput != Vector2.zero)
            {
                OnAxisChange?.Invoke(axisInput);
            }
            else
            {
                OnAxisButtonUp?.Invoke();
            }
            
        }

        private void CheckButtons()
        {
            if (SimpleInput.GetButtonDown(attackButtonName))
            {
                OnAttackButtonPress?.Invoke();
            }
        }
    
        public void Tick()
        {
            CheckButtons();
            CheckAxisChange();
        }

    
    }
}
