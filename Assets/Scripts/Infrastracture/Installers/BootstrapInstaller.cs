using Configs;
using Infrastracture.Services;
using Managers;
using UI;
using UnityEngine;
using Zenject;

namespace Infrastracture.Installers
{
   public class BootstrapInstaller : MonoInstaller
   {
      [SerializeField] private InteractionCanvas _interactionCanvas;
      [SerializeField] private CropsConfig _cropsConfig;
      public override void InstallBindings()
      {
         Container.BindInstance(_cropsConfig);
         Container.BindInterfacesAndSelfTo<InputService>().AsSingle().NonLazy();
         Container.BindInterfacesAndSelfTo<CropCreationManager>().AsSingle().Lazy();
         Container.BindInstance(_interactionCanvas);
      }
   }
}
