using System.Collections;
using System.Collections.Generic;
using Infrastracture.Factories;
using Interactable.Crops;
using UnityEngine;

public class CropCreator : GameObjectsCreator
{
    public CropCreator(string prefabPath) : base(prefabPath)
    {
    }

    public CropCreator(GameObject prefab) : base(prefab)
    {
    }
}
