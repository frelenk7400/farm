using UnityEngine;
using UnityEngine.Pool;

namespace Infrastracture.Factories
{
    public class GameObjectsCreator
    {
        private readonly GameObject _prefab;
        private readonly string _prefabPath;
        private readonly bool _isInitializedWithPath;
        public ObjectPool<GameObject> pool { get; }

        protected GameObjectsCreator(string prefabPath)
        {
            pool = new ObjectPool<GameObject>(FactoryMethod,Init,OnRelease);
            _prefabPath = prefabPath;
            _isInitializedWithPath = true;
        }

        protected GameObjectsCreator(GameObject prefab)
        {
            pool = new ObjectPool<GameObject>(FactoryMethod,Init,OnRelease);
            _prefab = prefab;
        }

        public GameObject FactoryMethod()
        {
            GameObject prefabToInstantiate = _isInitializedWithPath 
                ? Resources.Load<GameObject>(_prefabPath) 
                : _prefab;
            
            GameObject gameObject = Object.Instantiate(prefabToInstantiate);

            gameObject.name = Random.Range(0, 100000).ToString();

            return gameObject;
        }

        protected void Init(GameObject gameObject) =>
            gameObject.SetActive(true);

        protected void OnRelease(GameObject gameObject) =>
            gameObject.SetActive(false);
    }
}
