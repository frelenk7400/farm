namespace StaticVariables.Enums
{
    public enum FarmlandState
    {
        Empty = 0,
        Seed,
        Water,
        Gather
    }
}