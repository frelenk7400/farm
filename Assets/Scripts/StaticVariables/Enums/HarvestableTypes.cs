namespace StaticVariables.Enums
{
    public enum HarvestableType
    {
        Carrot,
        Apple
    }
}