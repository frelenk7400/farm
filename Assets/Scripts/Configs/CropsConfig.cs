using System.Collections.Generic;
using System.Linq;
using Interactable.Crops;
using StaticVariables.Enums;
using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "CropsConfig", menuName = "ScriptableObjects/CropConfigs", order = 1)]
    public class CropsConfig : ScriptableObject
    {
        [SerializeField] private List<CropsData> _cropsData;

        public CropsData GetCropData(HarvestableType harvestableType) =>
            _cropsData.FirstOrDefault(x => x.harvestableType == harvestableType);

        public List<CropsData> GetAllCrops() => new(_cropsData);

        [System.Serializable]
        public struct CropsData
        {
            public HarvestableType harvestableType;
            public GameObject cropPrefab;
            public GameObject cropFinalForm;
        }
    }
}
