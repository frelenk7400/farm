using Infrastracture.Services;
using UnityEngine;
using Zenject;

public class CharacterAnimatorController : MonoBehaviour
{
    private static readonly int Speed = Animator.StringToHash("Speed");
    
    [SerializeField] private CharacterController characterController;
    [SerializeField] private Animator animator;
    
    private IInputService _inputService;

    [Inject]
    public void Construct(IInputService inputService)
    {
        _inputService = inputService;
        _inputService.OnAxisChange += ChangeVelocity;
    }

    private void ChangeVelocity(Vector2 axis)
    {
        animator.SetFloat(Speed, characterController.velocity.magnitude);
    }
}
