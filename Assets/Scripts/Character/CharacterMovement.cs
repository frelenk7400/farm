using Infrastracture.Services;
using UnityEngine;
using Zenject;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private Transform virtualCamera;
    [SerializeField] private float speed;
    [SerializeField] private CharacterController characterController;
    private IInputService _inputService;
    private Transform cameraTransform;

    private Vector2 startRotateValue;
    private Vector3 previousCharacterRotation;
    
    private void Start()
    {
        cameraTransform = Camera.main.transform;
    }

    [Inject]
    public void Construct(InputService inputService)
    {
        _inputService = inputService;
        _inputService.OnAxisChange += Move;
        _inputService.OnAxisChange += Rotate;
        _inputService.OnAxisButtonUp += OnAxisUp;
    }

    private void OldMove(Vector2 moveAxis)
    {
        Vector3 movementValue = cameraTransform.TransformDirection(moveAxis);
        movementValue.y = 0;
        Vector3.Scale(moveAxis, transform.forward);
        characterController.Move(movementValue * Time.deltaTime * speed);
    } 
    private void Move(Vector2 moveAxis)
    {
        Vector3 movementValue = transform.forward * moveAxis.magnitude;
        
        characterController.Move(movementValue * Time.deltaTime * speed);
    }

    private void Rotate(Vector2 moveAxis)
    {
        if (startRotateValue == Vector2.zero)
        {
            startRotateValue = moveAxis;
            return;
        }

        Vector3 eulerAngle = new Vector3(0, -Vector2.SignedAngle(startRotateValue, moveAxis), 0);

        Vector3 newRotation = previousCharacterRotation + eulerAngle;
        
        transform.rotation = Quaternion.Euler(newRotation);
        
        Debug.Log(Vector2.SignedAngle(startRotateValue,moveAxis));
    }

    private void OnAxisUp()
    {
        startRotateValue = Vector3.zero;
        previousCharacterRotation = transform.rotation.eulerAngles;
    }
    
    private void OldRotate(Vector2 moveAxis)
    {
        float lookAngle = Vector2.SignedAngle(moveAxis, Vector2.up);

        lookAngle = lookAngle < 0 ? 360 + lookAngle : lookAngle;
        
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, lookAngle, transform.eulerAngles.z);
    }
    
}
