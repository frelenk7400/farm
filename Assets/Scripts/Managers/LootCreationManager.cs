using System.Collections.Generic;
using Configs;
using Infrastracture.Factories;
using StaticVariables.Enums;

public class LootCreationManager
{
    private readonly CropsConfig _cropConfig;
    private Dictionary<HarvestableType, GameObjectsCreator> cropCreator;

    public LootCreationManager(CropsConfig cropsConfig)
    {
        _cropConfig = cropsConfig;
    }
    
    public void Initialize()
    {
        cropCreator = new Dictionary<HarvestableType, GameObjectsCreator>();

        var allCrops = _cropConfig.GetAllCrops();
        foreach (var cropData in allCrops)
        {
            cropCreator.Add(cropData.harvestableType, new CropCreator(cropData.cropPrefab.gameObject));
        }
    }
    
   // public 
}
