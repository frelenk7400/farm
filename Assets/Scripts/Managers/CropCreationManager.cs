using System.Collections.Generic;
using Configs;
using Infrastracture.Factories;
using Interactable.Crops;
using StaticVariables.Enums;
using UnityEngine;
using Zenject;

namespace Managers
{
    public class CropCreationManager : IInitializable
    {
        private readonly CropsConfig _cropConfig;
        private Dictionary<HarvestableType, GameObjectsCreator> cropCreator;

        public CropCreationManager(CropsConfig cropsConfig)
        {
            _cropConfig = cropsConfig;
        }
    
        public void Initialize()
        {
            cropCreator = new Dictionary<HarvestableType, GameObjectsCreator>();

            var allCrops = _cropConfig.GetAllCrops();
            foreach (var cropData in allCrops)
            {
                cropCreator.Add(cropData.harvestableType, new CropCreator(cropData.cropPrefab.gameObject));
            }
        }

        public CropData GetCrop(HarvestableType harvestableType)
        {
            var cropsData = _cropConfig.GetCropData(harvestableType);
            GameObject cropGameObject = cropCreator[harvestableType].pool.Get();
            Crop crop = new Crop(cropGameObject, cropsData.cropFinalForm, harvestableType);
            CropData cropData = new CropData(crop, cropGameObject);
            return cropData;
        }

        public void ReleaseCropGameObject(HarvestableType harvestableType, GameObject gameObject) =>
            cropCreator[harvestableType].pool.Release(gameObject);

        public struct CropData
        {
            public GameObject cropGameObject;
            public IInteractable interactable;

            public CropData(Crop interactable, GameObject cropGameObject)
            {
                this.cropGameObject = cropGameObject;
                this.interactable = interactable;
            }
        }
    }
}
